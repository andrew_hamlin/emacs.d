;;; javascript-setup.el -- Setup javascript editing
;;; Commentary:
;;; Code:

;;(require 'auto-complete)
;;(add-to-list 'ac-modes 'js3-mode)

;;(add-hook 'js3-mode-hook
;;          (lambda ()
;;            (tern-mode t)
;;            (linum-mode 1)))

;; enable tern in js2-mode
(add-hook 'js-mode-hook
          (lambda ()
            (tern-mode t)
            (linum-mode 1)))

(with-eval-after-load 'tern
  (add-to-list 'company-backends 'company-tern))

;;      (require 'tern-auto-complete)
;;      (tern-ac-setup)))

;; Force restart of tern in new projects
;; $ M-x delete-tern-process
(defun delete-tern-process ()
  "Force restart of tern in new project."
  (interactive)
  (delete-process "Tern"))

(provide 'javascript-setup)
;;; javascript-setup.el ends here
