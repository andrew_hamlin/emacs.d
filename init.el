;;; init.el -- Emacs initialization
;;; Commentary:
;;; Load all my custom files.


;;; Code:

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(setq custom-file (locate-user-emacs-file "packages-setup.el"))
(load custom-file)

;; system setup
(setq custom-file (locate-user-emacs-file "system-setup.el"))
(load custom-file)

;; MOVED TO ~/.user.el
;; Custom set variables
;(setq custom-file (locate-user-emacs-file "custom.el"))
;(load custom-file)

;; Load company mode
(setq custom-file (locate-user-emacs-file "company-setup.el"))
(load custom-file)

;; Load lsp mode
(setq custom-file (locate-user-emacs-file "lsp-setup.el"))
(load custom-file)

;; Load flycheck mode
(setq custom-file (locate-user-emacs-file "flycheck-setup.el"))
(load custom-file)

;; Load Javascript mode
(setq custom-file (locate-user-emacs-file "javascript-setup.el"))
(load custom-file)

;; Load Json mode
(setq custom-file (locate-user-emacs-file "json-setup.el"))
(load custom-file)

;; Load Python mode
(setq custom-file (locate-user-emacs-file "python-setup.el"))
(load custom-file)

;; Load Haskell mode
(setq custom-file (locate-user-emacs-file "haskell-setup.el"))
(load custom-file)

;; Load Elm mode
(setq custom-file (locate-user-emacs-file "elm-setup.el"))
(load custom-file)

;; ----------- CLOJURE MODE ---------------
;; requires packages: clojure-mode cider
;;(eval-after-load 'flycheck '(flycheck-clojure-setup))

(setq custom-file (locate-user-emacs-file "~/.user.el"))
(load custom-file)

(provide 'init)
;;; init.el ends here
