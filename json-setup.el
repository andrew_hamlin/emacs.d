;;; json-setup.el -- Configure json-mode
;;; Commentary:
;;; Code:
(add-to-list 'auto-mode-alist '("\\.tern-project\\'" . json-mode))
(add-to-list 'auto-mode-alist '("\\.jshintrc\'" . json-mode))


;;; json-setup.el ends here
