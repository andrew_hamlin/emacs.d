(require 'lsp-mode)
(require 'company-setup)
(require 'company-lsp)

(with-eval-after-load 'company
  (add-to-list 'company-backends 'company-lsp))

(provide 'lsp-setup)
