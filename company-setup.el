;;; company-setup.el -- install company mode
;;; Commentary:
;;; Code:

(add-hook 'after-init-hook 'global-company-mode)

;; more backends: https://github.com/company-mode/company-mode/wiki/Third-Party-Packages

;; individual backends are added from individual setup files

(setq company-minimum-prefix-length 1
      company-idle-delay 0.0) ;; change default from 0.2

(provide 'company-setup)
;;; company-setup.el ends
