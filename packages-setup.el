;;; packages-setup.el --- Summary
;;; Commentary:
;;; Setup stable melpa repository

;;; Code:
(require 'package)

(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  (when no-ssl
    (warn "\
Your version of Emacs does not support SSL connections,
which is unsafe because it allows man-in-the-middle attacks.
There are two things you can do about this warning:
1. Install an Emacs version that does support SSL and be safe.
2. Remove this warning from your init file so you won't see it again."))
  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  ;;(add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  (add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
  (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
    (add-to-list 'package-archives (cons "gnu" (concat proto "://elpa.gnu.org/packages/")))))

(package-initialize)

;; ------------- PACKAGE MAINTENANCE ------------
;; begin my-packages routine
(require 'cl)
(defvar my-packages
  '(better-defaults
    company
    company-tern
    company-ghc
    company-jedi
    company-lsp
    elm-mode
    exec-path-from-shell
    flycheck
    ghc
    haskell-mode
    hi2
    jedi
    js2-mode
    js2-refactor
    json-mode
    less-css-mode
    lsp-mode
    lsp-ui
    markdown-mode
    projectile
    python-environment
    pyvenv
    tern
    )
  "My list of packages to be installed")

;; see if this machine needs any required packages
(defun my-packages-installed-p ()
  (loop for p in my-packages
        when (not (package-installed-p p)) do (return nil)
        finally (return t)))

(unless (my-packages-installed-p)
  ;; check for new packages
  (package-refresh-contents)
  ;; install missing packages
  (dolist (p my-packages)
    (unless (package-installed-p p)
      (package-install p))))

;;(provide 'my-packages)
;; end my-packages
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
