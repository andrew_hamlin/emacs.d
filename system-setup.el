;;; system-setup.el -- system wide setup

;;; Commentary:
;;; Setup for global settings
;;; emacs must be compiled with TrueType support
;;; on Gentoo Linux the USE flag `xft' and `gsettings'

;;; Code:

;; set exec-path from terminal PATH
(require 'exec-path-from-shell)
(exec-path-from-shell-initialize)

(setq auto-save-file-name-transforms `((".*" ,temporary-file-directory t))
      backup-directory-alist `((".*" . ,temporary-file-directory)))

;; key bindings
;; see http://www.emacswiki.org/emacs/EmacsForMacOS#toc20
(when (eq system-type 'darwin) ;; mac specific settings
  (setq mac-option-modifier 'alt)
  (setq mac-command-modifier 'meta)
  (global-set-key [kp-delete] 'delete-char) ;; sets fn-delete to be right-delete
  )

(setq inhibit-startup-screen t)
(menu-bar-mode t)

;(set-face-attribute 'default nil :height (* 11 10))

;; setup indent-buffer function
(defun indent-buffer ()
  "Mark entire region and `indent-region`."
  (interactive)
  (save-excursion
    (indent-region (point-min) (point-max) nil)))
(global-set-key [f12] 'indent-buffer)

;; setup local path for custom packages
(add-to-list 'load-path "~/.emacs.d/lisp")

(provide 'system-setup)
;;; system-setup.el ends here
