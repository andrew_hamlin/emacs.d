;;; python-setup.el -- configure Python mode
;;; Commentary:
;;; Code:

(require 'lsp-setup)
(load-library "auto-virtualenv")

;;(with-eval-after-load 'company
;;  (add-to-list 'company-backends 'company-jedi))

(add-hook 'python-mode-hook
	  (lambda() ()
	    (lsp)
	    ;;(lsp-deferred)
	    ;;(eglot-ensure)
	    (linum-mode 1)
	    (auto-virtualenv-set-virtualenv)
	  ))

;; Activate on changing buffers
(add-hook 'window-configuration-change-hook #'auto-virtualenv-set-virtualenv)
;; Activate on focus in
(add-hook 'focus-in-hook #'auto-virtualenv-set-virtualenv)


(provide 'python-setup)
;;; python-setup.el ends here
