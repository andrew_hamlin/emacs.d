;; elm-setup.el --- load elm-lang capabilities

;;; Commentary:
; http://www.lambdacat.com/post-modern-emacs-setup-for-elm/
; https://github.com/jcollard/elm-mode

;;; Code:

(require 'elm-mode)
(require 'lsp-setup)

(add-hook 'elm-mode-hook
          (lambda ()
            (lsp)
	    ;;(lsp-deferred)
            (elm-format-on-save-mode)
	    (linum-mode 1)
            ))

(provide 'elm-setup)
;;; elm-setup ends here
