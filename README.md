# Emacs setup

Emacs setup supporting Javascript, Angular, Elm, Node, Python, and Haskell.

Package dependencies are specified and installed automatically, see packages-setup.el.
- company
- company-tern
- company-ghc
- company-jedi
- company-lsp
- elm-mode
- flycheck
- ghc
- haskell-mode
- hi2
- jedi
- js2-mode
- js2-refactor
- json-mode
- less-css-mode
- lsp-mode
- python-environment
- pyvenv
- tern

I have chose to use company mode, [comp]lete [any]thing, in Emacs, using Tern and elm-oracle for completion suggestions (both are nodeJS packages) for Javascript and Elm, respectively. And am using eslint for Javascript linting behavior.

## Javascript, Angular, Node

Pre-requisites:

* NodeJs
* Node packages (globally)
  * tern
  * eslint
  * eslint-config-angular
  * eslint-plugin-angular
  
Install node packages

```
$ npm install -g tern eslint eslint-config-angular eslint-plugin-angular
```

## Elm

Setup Elm including all the available integrations, see https://github.com/jcollard/elm-mode and http://www.lambdacat.com/post-modern-emacs-setup-for-elm/, see the notes on elm-mode required for upgrade to elm-0.19.

Pre-requisites (for Emacs setup of formatting and auto-completion):

* ~~elm-oracle~~
* elm-format
* elm-language-server

**Note** elm-format must be [installed manually](https://github.com/avh4/elm-format/releases/tag/0.8.3) on my Gentoo box, the npm installer is facing the same permissions issues as the elm binary. 

```
$ npm install -g @elm-tooling/elm-language-server
```
  
## Python

Pre-requisites:

* Python (v3)
* pipenv (or virtualenv)
* jedi
* python-language-server

Install python3 packages:

```
$ pip install pipenv
$ pip install 'python-language-server[yapf]'
$ pip install jedi
```

FYI included in the git repo is the manually installed [auto-virtualenv](https://github.com/marcwebbie/auto-virtualenv). 

In emacs, one-time setup

```
M-x jedi:install-server
```

## Haskell

Todo

## Fonts

Locally I make sure to compile emacs with xft and gsettings.

The `gsettings` USE flag enables using Gnome's selected fonts so I removed the custom font loading code.

