;; ----------- HASKELL MODE ---------------
;; not sure if this is necessary any more?
;;(add-to-list 'load-path "~/.cabal/share/x86_64-osx-ghc-7.8.3/ghc-mod-5.4.0.0/")

;; turn on haskell mode
;;(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
;;(add-hook 'haskell-mode-hook 'turn-on-hi2)
(add-hook 'haskell-mode-hook
          (lambda ()
            (turn-on-haskell-indentation)
            (turn-on-hi2)
            (linum-mode 1)))

;; use F8 to navigate to imports
(with-eval-after-load 'haskell-mode
  '(progn
     (add-to-list 'company-backends 'company-ghc)
     (define-key haskell-mode-map [f8] 'haskell-navigate-imports)))
 
;; add ~/.cabal/bin to PATH
;;(let ((my-cabal-path (expand-file-name "~/.cabal/bin")))
;;  (setenv "PATH" (concat my-cabal-path path-separator (getenv "PATH")))
;;  (add-to-list 'exec-path my-cabal-path))

(eval-after-load 'haskell-mode '(progn
  (define-key haskell-mode-map (kbd "C-c C-l") 'haskell-process-load-or-reload)
  (define-key haskell-mode-map (kbd "C-c C-z") 'haskell-interactive-switch)
  (define-key haskell-mode-map (kbd "C-c C-n C-t") 'haskell-process-do-type)
  (define-key haskell-mode-map (kbd "C-c C-n C-i") 'haskell-process-do-info)
  (define-key haskell-mode-map (kbd "C-c C-n C-c") 'haskell-process-cabal-build)
  (define-key haskell-mode-map (kbd "C-c C-n c") 'haskell-process-cabal)
  (define-key haskell-mode-map (kbd "SPC") 'haskell-mode-contextual-space)))

(eval-after-load 'haskell-cabal '(progn
  (define-key haskell-cabal-mode-map (kbd "C-c C-z") 'haskell-interactive-switch)
  (define-key haskell-cabal-mode-map (kbd "C-c C-k") 'haskell-interactive-mode-clear)
  (define-key haskell-cabal-mode-map (kbd "C-c C-c") 'haskell-process-cabal-build)
  (define-key haskell-cabal-mode-map (kbd "C-c c") 'haskell-process-cabal)))

;; compile in the background
(autoload 'ghc-init "ghc" nil t)
(autoload 'ghc-debug "ghc" nil t)
(add-hook 'haskell-mode-hook (lambda () (ghc-init)))
